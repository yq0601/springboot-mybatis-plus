package com.ymy;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ymy.entity.Users;
import com.ymy.entity.enums.SexEnum;
import com.ymy.service.IUsersService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@Slf4j
public class MybatisPlusTest {

    @Autowired
    private IUsersService usersService;

    /**
     * 锁对象
     */
    private static final String lock = "lock";

    /**
     * 状态
     */
    private volatile   Integer status = 0;

    /**
     * 插入操作（使用mybatis-plus自带方法）
     */
    @Test
    public void add() {
        //初始化实例并赋值
//        Users user = Users.builder().uName("张三").uMail("123@163.com")
//                .uPassword("123456").uPhone("15000000000")
//                .createTime(LocalDateTime.now()).uSex(SexEnum.MAN).deleted(0).build();
//        //插入
//        boolean save = usersService.save(user);
//
//        log.info("保存返回的结果：{}",save);
    }

    /**
     * 查询操作（使用mybatis-plus自带方法）
     */
    @Test
    public void select() {

        //查询整张表
        List<Users> list = usersService.list();
        log.info("查询整张表的数据结果：{}", list);
        //通过用户id查询用户
        Users user = usersService.getById(1);
        log.info("通过用户id查询数据结果：{}", user);

    }

    /**
     * 修改操作（使用mybatis-plus自带方法）
     */
    @Test
    public void update() {

//        Users user = Users.builder().id(1).uName("张三").uMail("123123@163.com")
//                .uPassword("123123").uPhone("15000000000")
//                .createTime(LocalDateTime.now()).uSex(SexEnum.MAN).deleted(0).build();
//        //通过id删除记录
//        boolean flag = usersService.updateById(user);
//
//        log.info("根据id修改数据返回的结果：{}",flag);

    }


    /**
     * 删除操作（使用mybatis-plus自带方法）
     */
    @Test
    public void remove() {
        //通过id删除记录
        boolean flag = usersService.removeById(4);
        log.info("根据id修改数据返回的结果：{}", flag);

    }

    /**
     * 条件构造
     */
    @Test
    public void conditionalConstruct() {
        //通过用户名和性别查询用户信息
        QueryWrapper<Users> usersQueryWrapper = new QueryWrapper<Users>()
                .eq("u_name", "张三")//姓名
                .eq("u_sex", 1);//性别
        List<Users> users = usersService.list(usersQueryWrapper);
        log.info("根据用户名和性别查询的结果：{}", users);


        //将用户名为张三并且性别为男的用户的密码设置为999999
        UpdateWrapper<Users> updateWrapper = new UpdateWrapper<Users>()
                .eq("u_name", "张三")//姓名
                .eq("u_sex", 1)
                .set("u_password", "999999");//将密码设置为999999
        boolean update = usersService.update(updateWrapper);
        log.info("将用户名为张三并且性别为男的用户的密码设置为999999的返回结果：{}", update);


        //通过对象类的实行修改
        UpdateWrapper<Users> updateEntity = new UpdateWrapper<Users>()
                .eq("u_name", "张三")//姓名
                .eq("u_sex", 1);
        Users users1 = new Users();
        users1.setUPassword("999999");
        boolean update1 = usersService.update(users1, updateEntity);
        log.info("通过对象类的实行修改返回的结果：{}", update1);


        //删除用户名为王五并且性别为1的用户

        UpdateWrapper<Users> deleteUser = new UpdateWrapper<Users>()
                .eq("u_name", "王五")//姓名
                .eq("u_sex", 1);
        boolean remove = usersService.remove(deleteUser);
        log.info("删除用户名为王五并且性别为1的用户返回结果：{}", remove);


        //lambda表达式查询     查询用户名为zhangsan的用户
        LambdaQueryWrapper<Users> lambdaWrapper = new QueryWrapper<Users>().lambda().eq(Users::getUName, "张三");
        List<Users> lambdaUsers = usersService.list(lambdaWrapper);
        log.info("lambda表达式查询     查询用户名为zhangsan的用户返回结果：{}", lambdaUsers);
    }

    /**
     * 分页测试
     */
    @Test
    public void paginationTest() {

        //分页查询
        Page<Users> page = new Page<Users>(2, 5);// 1:当前页    5：每页展示多少行
        IPage<Users> usersPage = usersService.page(page);
        log.info("总数：{} 条", usersPage.getTotal());
        log.info("通过分页查询返回的结果条数：{}", usersPage.getRecords().size());
        log.info("通过分页查询返回数据：{}", usersPage);
        for (Users user : usersPage.getRecords()) {
            log.info("记录：{}", user);
        }

    }


    /**
     * 分页测试（手动编写sql）
     */
    @Test
    public void paginationSqlTest() {

        //分页查询
        Page<Users> page = new Page<Users>(1, 5);// 1:当前页    5：每页展示多少行
        IPage<Users> usersPage = usersService.selectList(page);
        log.info("总数：{} 条", usersPage.getTotal());
        log.info("通过分页查询返回的结果条数：{}", usersPage.getRecords().size());
        log.info("通过分页查询返回数据：{}", usersPage);
        for (Users user : usersPage.getRecords()) {
            log.info("记录：{}", user);
        }
    }


    /**
     * 逻辑删除
     */
    @Test
    public void logicRemove() {
        //删除用户名为令狐冲的用户信息
        boolean remove = usersService.remove(new UpdateWrapper<Users>().lambda().eq(Users::getUName, "令狐冲"));
        log.info("删除返回的结果：{}", remove);

    }


    /**
     * 枚举
     */
    @Test
    public void enumTest() {
        //删除用户名为令狐冲的用户信息
        Users users = usersService.getOne(new UpdateWrapper<Users>().lambda().eq(Users::getUName, "张三"));
        log.info("查询返回的结果：{}", users);
    }


    /**
     * 通用字段测试（插入）
     */
    @Test
    public void GeneralFieldInsertTest() {
        Users user = Users.builder().uName("东方不败").uMail("123@163.com")
                .uPassword("123456").uPhone("15000000000")
                .uSex(SexEnum.MAN).deleted(0)
                .build();
        //插入
        boolean save = usersService.save(user);
        log.info("保存返回的结果：{}", save);
    }

    /**
     * 通用字段测试（修改）
     */
    @Test
    public void GeneralFieldUpdateTest() {

        Users user = new Users();
        user.setUPassword("qoqoqoqoqoqoqo");
        //插入
        boolean save = usersService.update(user, new UpdateWrapper<Users>().lambda().eq(Users::getUName, "东方不败"));
        log.info("保存返回的结果：{}", save);
    }


    /**
     * 乐观锁测试
     */
    @Test
    public void versionTest() throws InterruptedException {
    //线程1
    Thread t1 = new Thread(() -> {
        updatePwd1();
    });
    //线程2
    Thread t2 = new Thread(() -> {
        updatePwd2();
    });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        log.info("执行完毕");
    }

    /**
     * 将张三的密码修改为：pppppppppppp
     *
     * @return
     */
    private void updatePwd1() {
        String threadName = Thread.currentThread().getName();
        Users users = usersService.getOne(new QueryWrapper<Users>().lambda().eq(Users::getUName, "张三"));
        log.info("线程：{} 获取到的用户信息：{}", threadName, users);
        try {
            //为了能让两个查询都执行完毕之后在执行修改操作，所以在这里休眠了1秒中
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        users.setUPassword("pppppppppppp");
        boolean update = usersService.updateById(users);
        log.info("线程：{} 修改了用户信息，状态：{}", threadName, update);
    }

    /**
     * 将张三的密码修改为：ooooooooooo
     *
     * @return
     */
    private void updatePwd2() {
        String threadName = Thread.currentThread().getName();
        Users users  = usersService.getOne(new QueryWrapper<Users>().lambda().eq(Users::getUName, "张三"));
        log.info("线程：{} 获取到的用户信息：{}", threadName, users);
        try {
            //为了能让两个查询都执行完毕之后在执行修改操作，所以在这里休眠了1秒中
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        users.setUPassword("ooooooooooo");
        boolean update = usersService.updateById(users);
        log.info("线程：{} 修改了用户信息，状态：{}", threadName, update);
    }


}
