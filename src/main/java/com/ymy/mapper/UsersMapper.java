package com.ymy.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ymy.entity.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yaomaoyang
 * @since 2020-03-10
 */
@Mapper
public interface UsersMapper extends BaseMapper<Users> {

    @Select("select * from users ")
    IPage<Users> selectListBySql(Page<Users> page);
}
