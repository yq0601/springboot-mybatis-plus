package com.ymy.entity.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * 性别枚举类
 */
public enum SexEnum  implements IEnum<Integer> {

    MAN(1,"男"),

    WOMAN(2,"女");

    @EnumValue
    private final int code;
    private final String descp;


    SexEnum(int code, String descp) {
        this.code = code;
        this.descp = descp;
    }


    @Override
    public String toString() {
        return this.descp;
    }
    @Override
    public Integer getValue() {
        return code;
    }

}
