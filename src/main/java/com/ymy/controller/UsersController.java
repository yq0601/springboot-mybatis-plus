package com.ymy.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yaomaoyang
 * @since 2020-03-10
 */
@RestController
@RequestMapping("/users")
public class UsersController {

}
