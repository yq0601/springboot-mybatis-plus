package com.ymy.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ymy.entity.Users;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yaomaoyang
 * @since 2020-03-10
 */
public interface IUsersService extends IService<Users> {

    /**
     * 通过手动编写sql查询分页
     * @param page
     * @return
     */
    IPage<Users> selectList(Page<Users> page);
}
